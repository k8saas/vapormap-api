FROM python:3.8
WORKDIR /vapormap
RUN git clone https://gitlab.com/vapormap/vapormap-app.git
WORKDIR /vapormap/vapormap-app/app
RUN pip install -r requirements/production.txt
ENV VAPOR_DBNAME=db_vapormap
ENV VAPOR_DBUSER=user_vapormap
ENV VAPOR_DBPASS=vapormap
ENV VAPOR_DBHOST=db
ENV DJANGO_SETTINGS_MODULE=vapormap.settings.production
EXPOSE 8001
CMD python manage.py makemigrations ; python manage.py migrate ; gunicorn vapormap.wsgi:application --bind 0.0.0.0:8001


